import { ModalController } from 'ionic-angular';
import { Component } from '@angular/core';

import { QuotePage } from './../quote/quote';
import { Quote } from './../../data/quote.interface';
import { QuotesProvider } from '../../providers/quotes'

@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html'
})
export class FavoritesPage {
  quotes: Quote[];

  constructor (private quotesService: QuotesProvider,
  private modalCtrl: ModalController){

  }

  ionViewWillEnter(){
    this.quotes = this.quotesService.getFavoriteQuotes();
  }

  onViewQuote(quote: Quote){
    const modal = this.modalCtrl.create(QuotePage, quote);
    modal.present();
    modal.onDidDismiss((remove: boolean) => {
      if (remove){
        this.onRemoveFromFavorites(quote);
      }
    });
  }

  onRemoveFromFavorites(quote: Quote){
    this.quotesService.removeQuoteFromFavorite(quote);
        
        /* There are two options to reload the array and "reload page" the page*/
        //this.quotes = this.quotesService.getFavoriteQuotes();
        const position = this.quotes.findIndex((quoteEl: Quote) => {
          return quoteEl.id == quote.id;
        });
        this.quotes.splice(position, 1);
  }
}