import { NavParams, AlertController } from 'ionic-angular';
import { Component, OnInit } from '@angular/core';

import { QuotesProvider } from './../../providers/quotes';
import { Quote } from "../../data/quote.interface";

@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html'
})
export class QuotesPage implements OnInit {
    quoteGroup: {category: string, quotes: Quote[], icon: string};
  
    constructor(
        private navParams: NavParams, 
        private alertCtrl: AlertController,
        private quotesProvider: QuotesProvider){
    
    }

    ngOnInit(){
        this.quoteGroup = this.navParams.data;
    }

    // ionViewDidLoad(){
    //   this.quoteGroup = this.navParams.data;
    //    using elvis (?), I use OnInit 'couse the html charge after this class, so is better use OnInit than use ViewDidLoad
    // }

    onAddToFavorite(selectedQuote: Quote){
        const alert = this.alertCtrl.create({
            title: 'add Quote',
            subTitle: 'Are you sure?',
            message: 'Are you sure you want to add the quote?',
            buttons:[
                {
                    text: 'Yes, go ahead',
                    handler: () => {
                        this.quotesProvider.addQuoteToFavorites(selectedQuote);
                    }
                },
                {
                    text: 'No, I changed my mind!',
                    role: 'cancel',
                    handler: () => {

                    }
                }
                
            ]
        });

        alert.present();
    }

    onRemoveFromFavorites(quote: Quote){
        this.quotesProvider.removeQuoteFromFavorite(quote);
    }
    isFavorite(quote: Quote){
        return this.quotesProvider.isQuotesFavorite(quote);
    }
}
